#include <iostream>
#include <climits>
using namespace std;


void xuatA(int m,int *a){
    for(int i=0;i<m;i++)
        for(int j=0;j<m;j++){
            cout<<"a["<<i<<"]["<<j<<"] = "<<a[i*m+j]<<endl;
        }
}

void xuatX(int n,int *x){
    for(int i=0;i<n;i++){
        cout<<"x["<<i<<"] = "<<x[i]<<endl;
    }
}

void nhap(int *m,int **a){
    cout<<"Nhap m = ";cin>>*m;
    *a = new int[(*m)*(*m)];
    for(int i=0;i<*m;i++){
        for(int j=0;j<*m;j++){
            cout<<"Nhap a["<<i<<"]["<<j<<"] = ";
            cin>>(*a)[i*(*m)+j];
        }
    }
}

int layMax(int pos, int m, int *a){
    int max=INT_MIN;
    for(int i=pos*m;i<(pos+1)*m;i++){
        if(max<a[i]) max=a[i];
    }
    return max;
}

void layX(int m,int *a, int **x){
    *x = new int[m+1];
    for(int i=0;i<m;i++){
        (*x)[i] = layMax(i,m,a);
    }
}

void sort(int m, int *a){
    for(int i=0;i<m;i++){
        for(int j=i+1;j<m;j++){
            if(a[j]<a[i]){
                int t=a[j];
                a[j]=a[i];
                a[i]=t;
            }
        }
    }
}

void timK(int m, int *x){
    int k;
    cout<<"Nhap so can tim K = ";
    cin>>k;
    for(int i=0;i<m;i++){
        if(x[i]==k){
            cout<<"Tim thay K="<<k<<" o vi tri x["<<i<<"]"<<endl;
            return;
        }
        if(x[i]>k){
            for(int j=m+1;j>i;j--){
                x[j]=x[j-1];
            }
            x[i]=k;
            cout<<"Khong tim thay K="<<k<<". Chen K vao vi tri x["<<i<<"]"<<endl;
            xuatX(m+1,x);
            return;
        }
    }
}


int main()
{
    int m,*a;
    nhap(&m,&a);
    xuatA(m,a);
    int *x;

    cout<<"Tim mang X:"<<endl;
    layX(m,a,&x);
    xuatX(m,x);

    cout<<"Sap xep X:"<<endl;
    sort(m,x);
    xuatX(m,x);

    timK(m,x);

    return 0;
}
