#include <iostream>

using namespace std;

int months[7] = {1,3,5,7,8,10,12};

int getMaxDay(int m, int y){
    if(m==2) return ((y%4==0 && y%100!=0) || y%400==0)?29:28;
    for(int i=0;i<7;i++)
        if(months[i]==m) return 31;
    return 30;
}

struct Date{
    int d,m,y;

    Date operator++(){
        Date newDate;
        newDate.d = d;
        newDate.m = m;
        newDate.y = y;

        int max = getMaxDay(m,y);
        if(d>=max){
            d=1;
            if(m>=12){
                m = 1;
                ++y;
            }
            else ++m;
        }
        else ++d;
        return newDate;
    }
    Date operator++( int ){
        int max = getMaxDay(m,y);
        if(d>=max){
            d=1;
            if(m>=12){
                m = 1;
                ++y;
            }
            else ++m;
        }
        else ++d;

        Date newDate;
        newDate.d = d;
        newDate.m = m;
        newDate.y = y;
        return newDate;
    }
    Date operator--(){
        Date newDate;
        newDate.d = d;
        newDate.m = m;
        newDate.y = y;

        if(d<=1){
            if(m<=1){
                m = 12;
                --y;
            }
            else --m;
            int max = getMaxDay(m,y);
            d = max;
        }
        else --d;
        return newDate;
    }
    Date operator--( int ){
        if(d<=1){
            if(m<=1){
                m = 12;
                --y;
            }
            else --m;
            int max = getMaxDay(m,y);
            d = max;
        }
        else --d;

        Date newDate;
        newDate.d = d;
        newDate.m = m;
        newDate.y = y;
        return newDate;
    }
};

istream &operator>>(istream &input, Date &date){
    input>>date.d>>date.m>>date.y;
    return input;
}

ostream &operator<<(ostream &output, Date &date){
    output<<"Ngay "<<date.d<<", Thang "<<date.m<<", Nam "<<date.y;
    return output;
}



int main()
{
    Date d;
    cout<<"Nhap Date:"<<endl;
    cin>>d;
    cout<<"Date: "<<d<<endl;

    d--;
    cout<<d<<endl;

    d++;
    cout<<d<<endl;

    return 0;
}

