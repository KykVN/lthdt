#include <iostream>

using namespace std;

struct Vector3D{
    int x,y,z;
};

istream &operator>>(istream &input, Vector3D &vector){
    input>>vector.x>>vector.y>>vector.z;
    return input;
}

ostream &operator<<(ostream &output, Vector3D &vector){
    output<<"X="<<vector.x<<" Y="<<vector.y<<" Z="<<vector.z;
    return output;
}

Vector3D operator+(Vector3D &a, Vector3D &b){
    Vector3D vector;
    vector.x = a.x+b.x;
    vector.y = a.y+b.y;
    vector.z = a.z+b.z;
    return vector;
}
Vector3D operator-(Vector3D &a, Vector3D &b){
    Vector3D vector;
    vector.x = a.x-b.x;
    vector.y = a.y-b.y;
    vector.z = a.z-b.z;
    return vector;
}

int operator*(Vector3D &a, Vector3D &b){
    return a.x*b.x+a.y*b.y+a.z*b.z;
}
int main()
{
    Vector3D a,b;
    cout<<"Nhap vector A:"<<endl;
    cin>>a;
    cout<<"Vector A: "<<a<<endl;

    cout<<"Nhap vector B:"<<endl;
    cin>>b;
    cout<<"Vector B: "<<b<<endl;

    Vector3D add = a+b;
    cout<<"A+B: "<<add<<endl;

    Vector3D sub = a-b;
    cout<<"A-B: "<<sub<<endl;

    double mul = a*b;
    cout<<"A*B="<<mul<<endl;
    return 0;
}
